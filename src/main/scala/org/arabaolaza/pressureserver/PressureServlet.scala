package org.arabaolaza.pressureserver

import org.arabaolaza.pressureserver.commands.commandsupport._
import org.arabaolaza.pressureserver.data.PersonData
import org.arabaolaza.pressureserver.model.Error
import org.json4s.DefaultFormats
import org.json4s.Formats
import org.scalatra.ScalatraServlet
import org.scalatra.commands.JacksonJsonParsing
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.json.JValueResult
import org.scalatra.validation.ValidationError
import org.scalatra.validation._
import scalaz.NonEmptyList
import org.scalatra.CorsSupport


class PressureServlet extends ScalatraServlet  with JacksonJsonParsing with JacksonJsonSupport with JValueResult with CorsSupport{
  
  protected implicit val jsonFormats: Formats = DefaultFormats
  
  
  before() {
    contentType = formats("json")
   
  }

  get("/") {
   PersonData.all
  }
  
  post("/"){    
   val cmd = command[CreatePersonCommand]
    PersonData.execute(cmd).fold(
      errors => processError(errors.list),
      person => PersonData.all
    )
  }
  
  get("/search"){
  val cmd = command[FindPersonCommand]
    PersonData.execute(cmd).fold(
      errors => processError(errors.list),
      person => person
    )
  }
  
  def processError(errors:List[ValidationError])={	
    errors head match{
	    case a@ValidationError(_,_,Some(UnknownError),_)=>halt(500,errors.map(error=>Error("There was a problem in the server",error.message,500)))
	    case b@ValidationError(_,_,Some(NotFound),_)=>halt(404,errors.map(error=>Error("Unknow Person",error.message,404)))  
	    case c@ValidationError(_,_,Some(ValidationFail),_)=>halt(400,errors.map(error=>Error("Validation Error","The field "+error.field.getOrElse("Unknow")+"is invalid: "+error.message,400)))  
	    case d@ValidationError(_,None,None,_)=>halt(500,errors.map(error=>Error("Unknow Error","Something happend but we don know what",500)))  
    }
	  
	    
  }
  
}
