package org.arabaolaza.pressureserver.data

import java.util.Date
import org.arabaolaza.pressureserver.commands.commandsupport.Logging
import org.arabaolaza.pressureserver.model.Person
import org.arabaolaza.pressureserver.model.Pressure
import scalaz._
import Scalaz._
import scala.util.control.Exception._
import org.scalatra.validation._
import org.scalatra.commands._
import java.util.concurrent.atomic.AtomicInteger
import org.arabaolaza.pressureserver.commands.commandsupport.CreatePersonCommand
import org.arabaolaza.pressureserver.commands.commandsupport.FindPersonCommand

object PersonData extends Logging with CommandHandler {

  var all = List(
    Person("Raul", "Arabaolaza", 33, List(Pressure(123, 80, new Date(System.currentTimeMillis())))),
    Person("Raul", "Arabaolaza", 33, List(Pressure(123, 80, new Date(System.currentTimeMillis())))),
    Person("Raul", "Arabaolaza", 33, List(Pressure(123, 80, new Date(System.currentTimeMillis())))))

  def add(person: Person): ModelValidation[Person] = {
    allCatch.withApply(errorFail) {
      all ::= person
      person.successNel
    }
  }

  def findByName(person: Person): ModelValidation[Person] = {
    allCatch.withApply(errorFail) {
      all find (p => p.firstName == person.firstName && p.lastName == person.lastName) match {
        case Some(desired) => desired.successNel
        case None => ValidationError("No person found with firstName "+person.firstName +" and lastName "+person.lastName, NotFound).failNel
      }

    }
  }

  protected def handle: Handler = {
    case c: CreatePersonCommand =>
      add(new Person(c.firstName.value getOrElse "", c.lastName.value getOrElse "", c.age.value.getOrElse(18), List[Pressure]()))
    case c: FindPersonCommand =>
      findByName(new Person(c.firstName.value getOrElse "", c.lastName.value getOrElse "",18,null))
  }
  def errorFail(ex: Throwable) = ValidationError(ex.getMessage, UnknownError).failNel

}