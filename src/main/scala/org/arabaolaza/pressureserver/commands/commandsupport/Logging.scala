package org.arabaolaza.pressureserver.commands.commandsupport

import grizzled.slf4j.Logger

trait Logging {
 
  @transient lazy val logger: Logger = Logger(getClass)

}