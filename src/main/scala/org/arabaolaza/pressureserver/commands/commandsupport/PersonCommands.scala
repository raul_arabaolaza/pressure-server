package org.arabaolaza.pressureserver.commands.commandsupport

import org.arabaolaza.pressureserver.model.Person

import org.scalatra.validation.Validators.{PredicateValidator, Validator}

// the Scalatra command handlers
import org.scalatra.commands._

// Scalatra's JSON-handling code
import org.scalatra.json._
import org.json4s.{DefaultFormats, Formats}



abstract class PersonCommand[S](implicit mf: Manifest[S]) extends ModelCommand[S]
  with JsonCommand


class CreatePersonCommand extends PersonCommand[Person] { 
  
  protected implicit val jsonFormats = DefaultFormats

  val firstName: Field[String] = asType[String]("firstName").notBlank.minLength(3)
  val lastName: Field[String] = asType[String]("lastName").notBlank.minLength(3)
  val age: Field[Int] = asType[Int]("age").greaterThan(5)

}

class FindPersonCommand extends PersonCommand[Person] { 
  
  protected implicit val jsonFormats = DefaultFormats

  val firstName: Field[String] = asType[String]("firstName").notBlank.minLength(3)
  val lastName: Field[String] = asType[String]("lastName").notBlank.minLength(3)

}